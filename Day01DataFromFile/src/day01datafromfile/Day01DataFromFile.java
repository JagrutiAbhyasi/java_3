package day01datafromfile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Day01DataFromFile {
    static final String DATA_FILE_NAME ="input.txt";
    
    //Scanner input = new Scanner(System.in);
    static ArrayList<String> nameList = new ArrayList<>();
    static ArrayList<Double> valList = new ArrayList<>();
        
    static void readDataFromFile () {
        Scanner fileInput = null;
        try {
            fileInput = new Scanner(new File(DATA_FILE_NAME));
            
        //try(fileInput = new Scanner(new File(DATA_FILE_NAME))){    
            while(fileInput.hasNextLine()){
                String line = fileInput.nextLine();
                try {
                    double val=Integer.parseInt(line);
                    valList.add(val);
                } catch (NumberFormatException e) {
                    nameList.add(line);
                }
                
            }
            
            
        } catch (IOException ex) {
            System.out.println("Error in reading file " + ex.getMessage());
        }finally{
            if(fileInput != null) {
                fileInput.close();
            }
        }
        
    }
    
    
    
    public static void main(String[] args) {
        readDataFromFile();
        Collections.sort(nameList);
        Collections.sort(valList);
        System.out.println("Names: " + String.join(", ", nameList));
        System.out.println("Numbers " + String.join(", ", valList.toString()));
        
    }
    
}
