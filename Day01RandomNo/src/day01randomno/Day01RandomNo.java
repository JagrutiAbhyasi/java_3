package day01randomno;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Day01RandomNo {

    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        
        try{
            
            System.out.println("How many Randon no you want to generate ? ");
            int RandNo = input.nextInt();

            System.out.println("Enter minimun No..");
            int minNo = input.nextInt();

            System.out.println("Enter maximun No..");
            int maxNo = input.nextInt();

            if (RandNo < 0 ){
                System.out.println("Please enter Positive No..");
                System.exit(1);
            }
            if(minNo > maxNo){
            System.out.println("Min No shuold be less than Max No..");
            System.exit(1);
            }
            
            String No1="";
        
            for (int i = 0; i < RandNo; i++) {
                int r = (int)(Math.random() * (maxNo - minNo + 1) + minNo);
                //No1 = No1 + ',' + r ;
                System.out.printf("%s%d",i ==0 ? "" : ", " , r);
            }
        
        //System.out.println("Random No is "  + No1);
            
        }catch(InputMismatchException ex){
            System.out.println("Error : value must be an integer");
        }
        
        
    }
    
}
